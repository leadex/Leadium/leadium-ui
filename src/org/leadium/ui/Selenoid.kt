package org.leadium.ui

import com.codeborne.selenide.WebDriverProvider
import org.junit.jupiter.api.extension.AfterEachCallback
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.BeforeEachCallback
import org.junit.jupiter.api.extension.ExtensionContext
import org.openqa.selenium.Dimension
import org.openqa.selenium.WebDriver
import org.openqa.selenium.logging.LogType
import org.openqa.selenium.logging.LoggingPreferences
import org.openqa.selenium.remote.CapabilityType
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.LocalFileDetector
import org.openqa.selenium.remote.RemoteWebDriver
import java.net.MalformedURLException
import java.net.URI
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.logging.Level

/**
 * The object creates a Remote Web Driver for Selenoid.
 * Object's canonical name is passed to the com.codeborne.selenide.Configuration.browser parameter
 * when running tests in parallel.
 * ```
 * Configuration.browser = Selenoid::class.java.canonicalName
 * ```
 */
object Selenoid : WebDriverProvider, BeforeEachCallback, AfterEachCallback, BeforeAllCallback {

    private val capabilities = DesiredCapabilities()
    private var videoUUID: String? = null
    private var isVideoEnabled: Boolean? = null

    override fun beforeAll(context: ExtensionContext?) {

        isVideoEnabled = context!!.element.get().isAnnotationPresent(EnableVideoRecording::class.java)
    }

    override fun createDriver(c: DesiredCapabilities): WebDriver {
        videoUUID = UUID.randomUUID().toString()

        fun loggingPreferences(): LoggingPreferences {
            with(LoggingPreferences()) {
                enable(LogType.BROWSER, Level.ALL)
                enable(LogType.CLIENT, Level.ALL)
                enable(LogType.DRIVER, Level.ALL)
                return this
            }
        }

        val dimensionWidth = System.getProperty("leadium.selenoid.dimensionWidth", "1280")!!.toInt()
        val dimensionHeight = System.getProperty("leadium.selenoid.dimensionHeight", "1024")!!.toInt()
        val selenoidOptions = mapOf(
            Pair("enableVNC", System.getProperty("leadium.selenoid.enableVNC", "true")!!.toBoolean()),
            Pair("enableVideo", isVideoEnabled),
            Pair("screenResolution", "${dimensionWidth}x${dimensionHeight}x24"),
            Pair("videoScreenSize", "${dimensionWidth}x${dimensionHeight}"),
            Pair("videoName", "video_$videoUUID.mp4")
        )

        capabilities.setCapability("browserName", System.getProperty("leadium.selenoid.browserName", "chrome"))
        capabilities.setCapability("browserVersion", System.getProperty("leadium.selenoid.version", "91.0"))
        capabilities.setCapability(CapabilityType.LOGGING_PREFS, loggingPreferences())
        capabilities.setCapability("selenoid:options", selenoidOptions)
        capabilities.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        try {
            val driver = RemoteWebDriver(
                URI.create("${host()!!}/wd/hub").toURL(),
                capabilities
            )
            driver.manage().window().size = Dimension(dimensionWidth, dimensionHeight)
            driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(60, TimeUnit.SECONDS);
            driver.fileDetector = LocalFileDetector()
            return driver
        } catch (e: MalformedURLException) {
            throw RuntimeException(e)
        }
    }

    private fun host() =
        if (System.getProperty("user.name").contains("jenkins"))
            System.getProperty("leadium.selenoid.host")
        else
            System.getProperty("leadium.selenoid.localhost")

    override fun beforeEach(context: ExtensionContext?) {
        capabilities.setCapability("name", context!!.displayName)
    }

//        override fun afterEach(context: ExtensionContext?) { //todo doesn't work =(
//            val parallel = System.getProperty("parallel", "false")!!.toBoolean()
//            val headless = System.getProperty("headless", "false")!!.toBoolean()
//            if (videoUUID != null && parallel && !headless)
//                attachHtml("Video", ("<html><body><video width='100%'"
//                        + " height='100%' controls autoplay><source src='"
//                        + "${host()!!}/video/video_$videoUUID.mp4"
//                        + "' type='video/mp4'></video></body></html>"))
//        }

    override fun afterEach(context: ExtensionContext?) {
        val parallel = System.getProperty("parallel", "false")!!.toBoolean()
        val headless = System.getProperty("headless", "false")!!.toBoolean()
        val video = context!!.element.get().isAnnotationPresent(AttachVideo::class.java)
        if (video && videoUUID != null && parallel && !headless)
            io.qameta.allure.Allure.getLifecycle().addAttachment(
                "Video",
                "text/html",
                ".html",
                "${host()!!}/video/video_$videoUUID.mp4".byteInputStream()
            )
    }
}

@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
annotation class EnableVideoRecording()

@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class AttachVideo()