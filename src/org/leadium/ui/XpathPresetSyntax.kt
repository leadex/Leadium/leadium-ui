package org.leadium.ui

interface XpathPresetSyntax {

    fun xpathByText(
        text: String,
        withText: Boolean = false,
        caseInsensitive: Boolean = false,
        last: Boolean = false
    ): String {

        /**
         * @suppress
         */
        fun labelIndex(last: Boolean): String {
            return if (last) "last()" else "1"
        }

        fun createXpath(text: String): String {
            val u = text.toUpperCase()
            val l = text.toLowerCase()
            return when (withText) {
                true ->
                    if (caseInsensitive)
                        "(//*[contains(translate(text(), '$u', '$l'), '$l')])[${labelIndex(last)}]"
                    else
                        "(//*[contains(text(), '$text')])[${labelIndex(last)}]"
                false ->
                    if (caseInsensitive)
                        "(//*[translate(text(), '$u', '$l')='$l'])[${labelIndex(last)}]"
                    else
                        "(//*[text()='$text'])[${labelIndex(last)}]"
            }
        }
        return createXpath(text)
    }
}