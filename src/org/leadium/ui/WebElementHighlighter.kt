package org.leadium.ui

import com.codeborne.selenide.WebDriverRunner
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.events.AbstractWebDriverEventListener
import java.lang.Thread.sleep

/**
 * The object highlights and marks web elements by java script.
 */
class WebElementHighlighter : AbstractWebDriverEventListener() {

    var highlighterMarkerJs = "arguments[0].setAttribute('style', 'background: #f8e85b; color: #c3122f');"

    fun elementIsWidget(webElement: WebElement): Boolean {
        return webElement.getAttribute("class").contains("Widget")
    }

    override fun beforeClickOn(element: WebElement?, driver: WebDriver?) {
        if (element != null && element.isDisplayed) {
            highlightElement(element)
        }
    }

    override fun beforeGetText(element: WebElement?, driver: WebDriver?) {
        if (element != null && element.isDisplayed) {
            markElement(element)
        }
    }

    fun highlightElement(webElement: WebElement) {
        try {
            val paint = highlighterMarkerJs
            val clean = "arguments[0].setAttribute('style', '');"
            if (!elementIsWidget(webElement)) {
                val jse = WebDriverRunner.getWebDriver() as JavascriptExecutor
                jse.executeScript(paint, webElement)
                sleep(100L)
                jse.executeScript(clean, webElement)
            }
        } catch (e: StaleElementReferenceException) {
            //
        }
    }

    fun markElement(
        webElement: WebElement,
        markerJs: String = "arguments[0].setAttribute('style', 'background: #E0E0E0');"
    ) {
        try {
            if (!elementIsWidget(webElement)) {
                val jse = WebDriverRunner.getWebDriver() as JavascriptExecutor
                jse.executeScript(markerJs, webElement)
            }
        } catch (e: StaleElementReferenceException) {
            //
        }
    }
}