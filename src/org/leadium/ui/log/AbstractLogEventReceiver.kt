package org.leadium.ui.log

import com.codeborne.selenide.logevents.LogEvent
import com.codeborne.selenide.logevents.LogEventListener
import org.leadium.ui.log.data.ReplacementDataTemplate

abstract class AbstractLogEventReceiver : LogEventListener {

    abstract val replacementTemplates: Array<ReplacementDataTemplate>

    /**
     * @suppress
     */
    override fun beforeEvent(logEvent: LogEvent?) {
        //
    }

    fun replaceValue(logEvent: LogEvent?, replacementTemplates: Array<ReplacementDataTemplate>): String? {
        with(replacementTemplates) {
            forEach {
                if (logEvent.toString().contains(it.oldValue)) {
                    return it.newValue
                }
            }
        }
        return logEvent.toString()
    }
}