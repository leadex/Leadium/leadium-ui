package org.leadium.ui.log

import com.codeborne.selenide.logevents.LogEvent
import org.apache.log4j.Logger
import org.leadium.ui.log.data.ReplacementDataTemplate

/**
 * The class collects main selenide logs.
 */
abstract class AbstractLogEventLogger(override val replacementTemplates: Array<ReplacementDataTemplate>) :
    AbstractLogEventReceiver() {

    private val log = Logger.getLogger(this.javaClass.simpleName)
    private var replacedLastLogEvent: String? = null

    /**
     * @suppress
     */
    override fun afterEvent(logEvent: LogEvent?) {
        val replacedCurrentLogEvent = replaceValue(logEvent, replacementTemplates)
        if (!replacedLastLogEvent.isNullOrEmpty() && replacedCurrentLogEvent == replacedLastLogEvent)
            log.info(".")
        else
            log.info(replacedCurrentLogEvent)
        replacedLastLogEvent = replacedCurrentLogEvent
    }
}