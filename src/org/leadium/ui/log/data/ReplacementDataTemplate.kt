package org.leadium.ui.log.data

/**
 * @suppress
 */
interface ReplacementDataTemplate {

    val oldValue: String
    val newValue: String?
}